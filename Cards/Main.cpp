
// Playing Cards
// AJ Vetter

#include <iostream>
#include <conio.h>

using namespace std;

enum CardSuit
{
	Diamonds,
	Clubs,
	Hearts,
	Spades
};

enum CardRank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	CardSuit Suit;
	CardRank Rank;
};

int main()
{


	(void)_getch();
	return 0;
}
